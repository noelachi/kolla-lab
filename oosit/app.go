package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"

	"github.com/noelachi/kollalab/utilities"
)

func main() {
	/**
		Check software requirements
	**/
	if runtime.GOOS != "linux" {
		log.Fatalf("This program is only for Linux systems")
	}
	utilities.VerifyRequirement("ansible-playbook")
	//utilities.VerifyRequirement("virt-install")

	/**
			Seting up the deployement host
	**/

	os.Setenv("LIBVIRT_DEFAULT_URI", "qemu:///system")
	var hosts []utilities.LibvirtVM
	var VMInstalled bool
	isoURL := "http://centos.crazyfrogs.org/7.7.1908/isos/x86_64/CentOS-7-x86_64-Minimal-1908.iso"
	userHome := os.Getenv("HOME")
	isoPath := userHome + "/Downloads/vms-base-disk.iso"
	progrHome := userHome + "/go/src/github.com/noelachi/kollalab/"
	configFile := progrHome + "playbooks/group_vars/all.yml"
	argsHostSetup := []string{"-v", "-i", progrHome + "playbooks/inventory/", "--vault-password-file=" + userHome + "/.ansible/vault_password",
		"--extra-vars", "@" + userHome + "/" + ".ansible/vault.yml", progrHome + "playbooks/setup-virtualization.yml"}
	argsDepSetup := []string{"-v", "-i", progrHome + "playbooks/inventory/", "--vault-password-file=" + userHome + "/.ansible/vault_password",
		"--extra-vars", "@" + userHome + "/" + ".ansible/vault.yml", progrHome + "playbooks/setup-deployement.yml", "--key-file", progrHome + "playbooks/ressources/id_rsa_lab.pem"}

	cmdHostSetup := exec.Command("ansible-playbook", argsHostSetup...)
	cmdHostSetup.Stdout = os.Stdout //connect output to stdout for live output visualization. Useful for Ansible
	cmdHostSetup.Stderr = os.Stderr

	// Download iso disk
	if _, err := os.Stat(isoPath); err == nil {
		fmt.Println("The iso file ", isoPath, "already exist. Skipping download.")

	} else if os.IsNotExist(err) {
		fmt.Println(isoPath, " not found. Downloading ...")
		utilities.DownloadFile(isoPath, isoURL)
	} else {
		log.Fatalf("Fatal error %s\n", err)

	}

	err2 := cmdHostSetup.Run()
	if err2 != nil {
		log.Fatalf("cmdHostSetup.Run() failed with %s\n", err2)
	}
	/**
			Decoding config file to get host names / Starting VMs
	**/

	ctlHs, cptHs, stoHs, momHs, dpHs := utilities.GetHostsConfig(configFile)

	for i := 0; i < len(ctlHs.Nodes); i++ {
		node := ctlHs.Nodes[i]
		hosts = append(hosts, node)
		if node.Exist() {
			fmt.Println(node.Name, " virtual machine already exist. Skipping ...")
		} else {
			VMInstalled = true
			node.CreateVM(progrHome+"playbooks/conf/", "ks."+node.Name+".cfg", isoPath)
			time.Sleep(20 * time.Second)
		}
	}

	for i := 0; i < len(cptHs.Nodes); i++ {
		node := cptHs.Nodes[i]
		hosts = append(hosts, node)
		if node.Exist() {
			fmt.Println(node.Name, " virtual machine already exist. Skipping ...")
		} else {
			VMInstalled = true
			node.CreateVM(progrHome+"playbooks/conf/", "ks."+node.Name+".cfg", isoPath)
			time.Sleep(20 * time.Second)
		}
	}

	for i := 0; i < len(stoHs.Nodes); i++ {
		node := stoHs.Nodes[i]
		hosts = append(hosts, node)
		if node.Exist() {
			fmt.Println(node.Name, " virtual machine already exist. Skipping ...")
		} else {
			VMInstalled = true
			node.CreateVM(progrHome+"playbooks/conf/", "ks."+node.Name+".cfg", isoPath)
			time.Sleep(20 * time.Second)

		}
	}

	for i := 0; i < len(momHs.Nodes); i++ {
		node := momHs.Nodes[i]
		hosts = append(hosts, node)
		if node.Exist() {
			fmt.Println(node.Name, " virtual machine already exist. Skipping ...")
		} else {
			VMInstalled = true
			node.CreateVM(progrHome+"playbooks/conf/", "ks."+node.Name+".cfg", isoPath)
			time.Sleep(20 * time.Second)
		}
	}

	for i := 0; i < len(dpHs.Nodes); i++ {
		node := dpHs.Nodes[i]
		hosts = append(hosts, node)
		if node.Exist() {
			fmt.Println(node.Name, " virtual machine already exist. Skipping ...")
		} else {
			VMInstalled = true
			node.CreateVM(progrHome+"playbooks/conf/", "ks."+node.Name+".cfg", isoPath)
			time.Sleep(20 * time.Second)
		}
	}

	/**
			Check if OS installation is ok for all VMs. To fix: if all vms exist, the script will wait anyway. To fixe with utilities.VMListExist.
	**/

	fmt.Println("\n", "*********** Waiting for vms to be ready *******************")
	var hostNames []string
	for i := range hosts {
		hostNames = append(hostNames, hosts[i].Name)
	}
	if VMInstalled {
		// the first argument is the base waiting time. eg 10 for 10 min
		if utilities.WaitForOSInstall(20, 2, hostNames) {
			fmt.Println("OS installation successful for all VMs")
		} else {
			log.Fatalln("OS installation waiting time exeeded without success")
		}
	}

	for i := 0; i < len(hosts); i++ {
		if hosts[i].Started() {
			fmt.Println(hosts[i].Name, " virtual machine has already started. Skipping ...")
		} else {
			hosts[i].Start()
			fmt.Println("Starting  virtual machine ", hosts[i].Name)
			time.Sleep(20 * time.Second)
		}
	}
	time.Sleep(60 * time.Second)
	/**
			Setup deployement host
	**/
	cmdDepSetup := exec.Command("ansible-playbook", argsDepSetup...)
	cmdDepSetup.Stdout = os.Stdout //connect output to stdout for live output visualization. Useful for Ansible
	cmdDepSetup.Stderr = os.Stderr
	err3 := cmdDepSetup.Run()
	if err3 != nil {
		log.Fatalf("cmdDepSetup.Run() failed with %s\n", err3)
	}

}
