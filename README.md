# Openstack Lab 
This lab is based on Kolla Ansible. It's a Golang + Ansile program that deploy a full OpenStack Cluster based on the Kolla Ansible tool.

## Requirements
- A machine running RHEL (tested on centos 8 but it may work with other RedHat based distros)
- Enough CPU and RAM (12 cores, 32+GB RAM)
- It is not required to know how kolla works but would be beneficial

## Configuration
The default config will spin up 3 control hosts, 2 compute hosts, 2 storage hosts and 1 monitoring host.
This lab provides block storage and object storage via Swift

You can edit the configuration to suit your needs. All you have to do is to alter the file playbooks/group_vars/all.yml

## Usage
- Install Ansible + Go on your machine and get yaml/v3 (`go get gopkg.in/yaml.v3`)
- `mkdir $GOPATH/src/github.com/noelaachi\kollalab` 
- `cd $GOPATH/src/github.com/noelaachi\kollalab && https://github.com/noelachi/kolla-lab.git .` 
- Create an ansible vaukt password in $home/.ansible/vault.yam and populate it with `root_passwd: YOUR_SUDO_PASSWD` and `root_passwd_vm: shroot`
- Edit the config file
- run via go run oosit


## Note
There is a manual action to do to get swift working. You have to manually configure the swift Ring on the Operator Host.
If you dont change the storage nodes IP and number, all is already set up. But if you do, you have to alter the plabooks/templates/swift.sh file accordingly.
You will need to change `STORAGE_NODES=(172.29.236.21 172.29.236.22)` and `0..1` to match the new config.

user/passwd for all machines are labuser/labuser1


