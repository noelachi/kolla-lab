package utilities

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"gopkg.in/yaml.v3"
)

//NetNames ... variable to designate networks that will be named in the lab i order
var NetNames = [4]string{"mgmt", "vxlan", "storage", "vlan"}

//Started ... check if a VM has started
func (v LibvirtVM) Started() bool {
	args := []string{"list", "--state-running", "--name"}
	cmd := exec.Command("virsh", args...)
	out, err := cmd.Output()
	if err != nil {
		log.Fatalf("virsh list --all --name failed with %s\n", err)
	}
	outS := strings.TrimSpace(string(out))
	outTable := strings.Split(outS, "\n")
	return (stringInSlice(string(v.Name), outTable))
}

//Start ... start VM
func (v LibvirtVM) Start() {
	args := []string{"start", string(v.Name)}
	cmd := exec.Command("virsh", args...)
	_, err := cmd.Output()
	if err != nil {
		log.Fatalln("Unable to start ", string(v.Name), "VM. Error :", err)
	}
}

//Stop ... gracfully stop VM
func (v LibvirtVM) Stop() {
	args := []string{"shutdown", string(v.Name)}
	cmd := exec.Command("virsh", args...)
	_, err := cmd.Output()
	if err != nil {
		log.Fatalln("Unable to start ", string(v.Name), "VM. Error :", err)
	}
}

//Exist ... check if a libvirt Vm already exist
func (v LibvirtVM) Exist() bool {
	args := []string{"list", "--all", "--name"}
	cmd := exec.Command("virsh", args...)
	out, err := cmd.Output()
	if err != nil {
		log.Fatalf("virsh list --all --name failed with %s\n", err)
	}
	outS := strings.TrimSpace(string(out))
	outTable := strings.Split(outS, "\n")
	return (stringInSlice(string(v.Name), outTable))
}

// CreateVM ... Create VM using virt-install native cmd
func (v LibvirtVM) CreateVM(initrdPath string, initrdFileName string, isoPath string) {
	argsVmsInstBase := []string{"--name", v.Name, "--memory", v.Memory, "--vcpus", v.Vcpus, "--os-variant", v.Variant,
		"--location", isoPath, "--noautoconsole", "--vnc", "--initrd-inject", initrdPath + initrdFileName, "--extra-args", "inst.ks=file:/" + initrdFileName, "--cpu", "host-passthrough", "-v"}

	for i := 0; i < len(v.Disks); i++ {
		argsVmsInstBase = append(argsVmsInstBase, "--disk", "pool=default,size="+v.Disks[i]+",bus=virtio")
	}

	for i := 0; i < len(NetNames); i++ {
		netName := NetNames[i]
		_, ok := v.Networks[netName]
		if ok {
			argsVmsInstBase = append(argsVmsInstBase, "--network", "network="+netName+",model=virtio")
		}
	}

	cmdVmsInstall := exec.Command("virt-install", argsVmsInstBase...)
	cmdVmsInstall.Stdout = os.Stdout
	cmdVmsInstall.Stderr = os.Stderr
	fmt.Println(argsVmsInstBase)
	err2 := cmdVmsInstall.Run()
	if err2 != nil {
		log.Fatalf("cmdVmsInstall.Run() failed with %s\n", err2)
	}
}

// LibvirtVM ...  structure to represente Libvirt VM.
type LibvirtVM struct {
	Name     string            `yaml:"name"`
	Disks    []string          `yaml:"disks"`
	Vcpus    string            `yaml:"vcpus"`
	Variant  string            `yaml:"host_variant"`
	Memory   string            `yaml:"memory"`
	Networks map[string]string `yaml:"networks"`
}

// ControlHosts ...  table of infra hosts.
type ControlHosts struct {
	Nodes []LibvirtVM `yaml:"control_hosts"`
}

// ComputeHosts ...  table of compute hosts.
type ComputeHosts struct {
	Nodes []LibvirtVM `yaml:"compute_hosts"`
}

// StorageHosts ...  table of storage hosts.
type StorageHosts struct {
	Nodes []LibvirtVM `yaml:"storage_hosts"`
}

// MonitoringHosts ...  table of monitoring hosts.
type MonitoringHosts struct {
	Nodes []LibvirtVM `yaml:"monitoring_host"`
}

// DepHosts ...  table of log hosts.
type DepHosts struct {
	Nodes []LibvirtVM `yaml:"dep_hosts"`
}

// GetHostsConfig ...  parses given yaml file to extract host configs by types. Argument : filename = config file returns: infras hosts list, compute hosts list, storage hosts list, lb host lists, log host lists
func GetHostsConfig(fileName string) (ControlHosts, ComputeHosts, StorageHosts, MonitoringHosts, DepHosts) {
	f, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	ctlHs := ControlHosts{}
	cptHs := ComputeHosts{}
	stoHs := StorageHosts{}
	monHs := MonitoringHosts{}
	dpHs := DepHosts{}

	err = yaml.Unmarshal(f, &ctlHs)
	if err != nil {
		log.Fatalf("error decoding infra: %v", err)
	}
	err = yaml.Unmarshal(f, &cptHs)
	if err != nil {
		log.Fatalf("error decoding compute: %v", err)
	}
	err = yaml.Unmarshal(f, &stoHs)
	if err != nil {
		log.Fatalf("error decoding storage: %v", err)
	}
	err = yaml.Unmarshal(f, &monHs)
	if err != nil {
		log.Fatalf("error decoding lb: %v", err)
	}
	err = yaml.Unmarshal(f, &dpHs)
	if err != nil {
		log.Fatalf("error decoding log: %v", err)
	}

	return ctlHs, cptHs, stoHs, monHs, dpHs
}

//tableXinYByElements ... checks if all the elements of table X are in table Y
func tableXinYByElements(X, Y []string) bool {
	if len(X) > len(Y) {
		return false
	}
	// create a map of string -> int
	refMap := make(map[string]int, len(Y))
	for _, yj := range Y {
		// Build a map from the first []string
		refMap[yj]++
	}
	for _, xi := range X {
		// Take every element of []string 2 and verify if it is in the first map
		if _, isIn := refMap[xi]; !isIn {
			return false
		}
		refMap[xi]--
		if refMap[xi] == 0 {
			delete(refMap, xi)
		}
	}
	if len(refMap) == 0 {
		return true
	}
	return false
}

//StringInSlice ... check if a value is in a table of strings
func stringInSlice(a string, list []string) bool {
	refMap := make(map[string]int, len(list))
	for _, i := range list {
		refMap[i]++
	}
	if _, isIn := refMap[a]; !isIn {
		return false
	}
	return true
}

//VMListExist ... check if a list of libvirt Vms already exist
func VMListExist(vms []string) bool {
	args := []string{"list", "--all", "--name"}
	cmd := exec.Command("virsh", args...)
	out, err := cmd.Output()
	if err != nil {
		log.Fatalf("virsh list --all --name failed with %s\n", err)
	}
	outS := strings.TrimSpace(string(out))
	outTable := strings.Split(outS, "\n")
	return (tableXinYByElements(vms, outTable))
}

//VmsStopped ... will verify if all the VMs has stopped
func VmsStopped(vms []string) bool {
	args := []string{"list", "--state-shutoff", "--name"}
	cmd := exec.Command("virsh", args...)
	out, err := cmd.Output()
	if err != nil {
		log.Fatalf("virsh list --state-shutoff --name failed with %s\n", err)
	}
	outS := strings.TrimSpace(string(out))
	outTable := strings.Split(outS, "\n")
	//	fmt.Println("Existing vms ", outTable)
	//	fmt.Println("Supplied vms ", vms)
	return tableXinYByElements(vms, outTable)
}

//DownloadFile ... to download files
func DownloadFile(filepath string, url string) error {

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

//WaitForOSInstall ... to check if OS install is OK. Total waiting time = wait + retry*3 in minutes
func WaitForOSInstall(wait int, retry int, vms []string) bool {
	i := 0
	time.Sleep(time.Duration(wait) * time.Minute)
	if VmsStopped(vms) {
		return true
	}
	for i < 3 {
		i++
		time.Sleep(time.Duration(retry) * time.Minute)
		if VmsStopped(vms) {
			return true
		}
	}
	return false
}

//VerifyRequirement ... test if a software requirement is satisfied
func VerifyRequirement(r string) {
	path, err := exec.LookPath(r)
	if err != nil {
		log.Fatalf(r, " not found on this system. Please install it.")
	} else {
		fmt.Println("virt-install found in ", path, ".")
	}
}
